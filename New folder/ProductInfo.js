import React, { Component } from 'react';
import Panel from 'react-bootstrap/lib/Panel'
import axios from 'axios';
import { Pane, Text, Heading, TextInput, Select, RadioGroup, Combobox, Button, Paragraph } from 'evergreen-ui';
import { Link } from 'react-router-dom';
import './App.css';
import { Alert } from 'antd';
import SweetAlert from 'react-bootstrap-sweetalert';
import ReactDOM from 'react-dom';

//This Component is a child Component of Customers Component
export class ProductInfo extends Component {
  displayName = ProductInfo.name

  constructor(props) {
    super(props);
    this.state = {
      ProductType: "",
      popAlert: null,
      data: {}, errorResult: [],createaccountresponse: [],createaccountERRORresponse: []
    };
  }


  togglePopup = (msg) => {
    const getAlert = () => (
      <SweetAlert
        success
        title="PSBank"
        onConfirm={() => this.hideAlert()}
      > {msg}
      </SweetAlert>
    );

    this.setState({
      popAlert: getAlert
    })
  }

  hideAlert() {
    this.setState({ popAlert: null })
  };

  uploadForm1 = (value, event) => {
    event.preventDefault();
    alert("Account created");
    const getAlert = () => (
      <SweetAlert
        success
        title="PSBank"
        onConfirm={() => this.hideAlert()}
      > Account Created.
  </SweetAlert>
    );

    this.setState({
      popAlert: getAlert()
    });
  };

  uploadForm = (value, event) => {
    event.preventDefault();
    this.setState({ ProductType: { value } });

    const config = {
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
      }
    };

    const form1 = {
      id: this.props.location.state.userid,
      lastName: this.props.location.state.lastname,
      gender: this.props.location.state.gender,
      documentType: this.props.location.state.documenttype,
      mobileNumber: this.props.location.state.mobilenumber,
      referenceNumber: this.props.location.state.applicationrefno,
      civilStatus: this.props.location.state.civilstatus,
      type: "I",
      presentAddress: {
        zipCode: this.props.location.state.zipcodepres,
        country: "PH",//this.props.location.state.countrypres,
        province: "PH-ABR",//this.props.location.state.provincepres,
        city: "6302",//this.props.location.state.citypres,
        address: this.props.location.state.streetpres
      },
      permanentAddress: {
        zipCode: this.props.location.state.zipcodeperm,
        country: "PH",//this.props.location.state.countryperm,
        province: "PH-ABR",//this.props.location.state.provinceperm,
        city: "6302",//city: this.props.location.state.cityperm,
        address: this.props.location.state.streetperm
      },
      incomeSource: "0001",//this.props.location.state.sourceincome,
      emailAddress: this.props.location.state.email,
      productType: this.state.ProductType,
      monthlyIncome: this.props.location.state.monthlyIncome,
      birthPlace: "PH-ABR",//this.props.location.state.placeofbirth,
      profession: "NA",
      requestor: "TESTUSER",
      birthDate: this.props.location.state.birthdate,
      firstName: this.props.location.state.firstname,
      nationality: "PH",
      responsibilityCode: "111",
      middleName: this.props.location.state.middlename,
      spouseName: this.props.location.state.spousename,
      fatca: "NO",
      accountFacility: this.props.location.state.accountfacilities
    };
    console.log(form1);

    axios.post('http://10.11.27.104:3001/api/webapi/createaccount', form1)
      .then(response => {
        this.setState({ createaccountresponse: response.data });
        if (Response.data.referenceNumber) {
          this.props.history.push({ pathname: '/endform', state: this.state.fields });
        } else {
          let results = null;

          if (typeof (response.data) === "string") {
            results = JSON.parse(response.data);
          } else {
            results = response.data;
          }
          this.setState({ errorResult: results.value });
          console.log(this.state.errorResult);
          alert(this.state.errorResult);
        }
      })
      .catch((ex) => {
        console.error(ex);
        this.setState({ isError: true });
       // this.setState({ createaccountERRORresponse: response.data.data("error") });
        alert();
        //this.setState({ createaccountresponse: 'gaga' });
      });    


  }//end uplodForm


  //async addTaskAsync(taskDescription) {
  //  const url = 'api/WebApi/CreateAccount';
  //  const response = await fetch(url, {
  //    method: 'POST',
  //    headers: {
  //      'Accept': 'application/json',
  //      'Content-Type': 'application/json'
  //    },
  //    body: { taskDescription }
  //  });
  //  if (!response.ok) {
  //    throw new Error(`TasksService.addTaskAsync failed, HTTP status ${response.status}`);
  //  }
  //  const data = await response.json();
  //  return data;
  //}

  render() {
    console.log(this.state);
    console.log(this.props.location.state);
    const { options, vFatka, vConfirmadd } = this.state;
    return (
      <div >
        {/* 1st Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <img src="/Savings.JPG" height="80" />
          </Pane>
        </Pane>
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Heading size={900} color="#1070CA" >Savings</Heading>
          </Pane>
        </Pane>

        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Paragraph>An account encourages people to save money. Our personal savings account offers a fixed interest rate and convenient ways to monitor transactions.</Paragraph>
          </Pane>
        </Pane>
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} height={100}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Button height={36} marginRight={16} appearance="primary" onClick={this.uploadForm.bind(this, "710PHP")} >Apply Now</Button>
          </Pane>
        </Pane>

        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <img src="/Checking.JPG" height="80" />
          </Pane>
        </Pane>
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Heading size={900} color="#1070CA" >Checking</Heading>
          </Pane>
        </Pane>

        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Paragraph>An account that provides.</Paragraph>
          </Pane>
        </Pane>
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} height={100}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Button height={36} marginRight={16} appearance="primary" onClick={this.uploadForm.bind(this, "299PHP")} >Apply Now</Button>
          </Pane>
        </Pane>


        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <img src="/TimeDeposit.JPG" height="80" />
          </Pane>
        </Pane>
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Heading size={900} color="#1070CA" >Time Deposit</Heading>
          </Pane>
        </Pane>

        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Paragraph>An account that helps clients.</Paragraph>
          </Pane>
        </Pane>
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} height={100}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Button height={36} marginRight={16} appearance="primary" onClick={this.uploadForm.bind(this, "901PHP")} >Apply Now</Button>
          </Pane>
        </Pane>
      </div>
    )
  }
}
