import React, { Component } from 'react';
import Panel from 'react-bootstrap/lib/Panel'
import axios from 'axios';
import { Pane, Text, Heading, TextInput, Select, SelectField, RadioGroup, Combobox, Button, Switch } from 'evergreen-ui';
import { Link } from 'react-router-dom';
import 'antd/dist/antd.css';
import { Radio, DatePicker } from 'antd';
import moment from 'moment';

//This Component is a child Component of Customers Component
export class ClientDetails extends Component {
  displayName = ClientDetails.name

  constructor(props) {
    super(props);
    this.loadProduct = this.loadProduct.bind(this)
    this.state = {
      options: [
        { label: 'Yes', value: 'Y' },
        { label: 'No', value: 'N' }
      ],
      vFatka: 'N',
      vConfirmadd: 'N',
      vmunicipality : '', municipality: '',municipalityTempList: [],municipalityTemp2List: [], 
      tempval: '',
      fields: {
        firstname: '',
        middlename: '',
        lastname: '',
        birthdate: '',
        mobilenumber: '',
        nationality: 'FILIPINO',
        placeofbirth: 'PH-ABR',
        email: '',
        unitnopres: '',
        bldgpres: '',
        unitnoperm: '',
        bldgperm: '',
        cif: '00000000',
        userid: 'TESTER',
        customertype: '',
        gender: 'M',
        civilstatus: 'M',
        spousename: 'NOT APPLICABLE',
        employername: 'EMPLOYER NAME',
        employeraddress: 'EMPLOYER ADDRESS',
        monthlyincome: '0',
        documenttype: '',
        producttype: '',
        rccode: '111',
        accountfacilities: '',
        fatcadeclaration: '',
        applicationrefno: '',
        streetpres: "",
        barangaypres: "n/a",
        citypres: "",
        provincepres: "PH-ABR",
        countrypres: "PH",
        zipcodepres: "",
        streetperm: "",
        barangayperm: "n/a",
        cityperm: "",
        provinceperm: "PH-ABR",
        countryperm: "PH",
        zipcodeperm: "",
        sourceincome: "0001",
        naturework: "0001"
      }
    }

    this.fieldProvinceChange = this.fieldProvinceChange.bind(this)
  }

  fieldOnChange = sender => {
    let fieldName = sender.target.name;
    let value = sender.target.value;
    let state = this.state;
    this.setState({
      ...state,
      fields: { ...state.fields, [fieldName]: value }
      ,
    });
    console.log(this.state.fields);
  }

fieldProvinceChange =sender => {
    let fieldName = sender.target.name;
    let value = sender.target.value;
    let state = this.state;
    let vselectedprov = value.substring(0,6)

    this.setState({
      ...state,
      fields: { ...state.fields, [fieldName]: value }
    });
   //this.setState({vurlgago: value.substring(0,6)});
   // this.setState({vurlgago: "http://10.11.27.104:3001/api/WebApi/GetMunicipality/"+ vdelimited});
    axios.all([
      axios.get('http://10.11.27.104:3001/api/WebApi/GetMunicipality/'+vselectedprov )
    ])
    .then(axios.spread((data2) => {
      this.setState({
         municipalityTempList: data2.data,  
      })}
      ))  
    
   // this.setState({ municipalityTempList: this.state.municipalityList });
    console.log(this.state.municipalityTempList);
  }

fieldProvinceChange2 =sender => {
  let fieldName = sender.target.name;
  let value = sender.target.value;
  let state = this.state;
  let vselectedprov = value.substring(0,6)

  this.setState({
    ...state,
    fields: { ...state.fields, [fieldName]: value }
  });
 //this.setState({vurlgago: value.substring(0,6)});
 // this.setState({vurlgago: "http://10.11.27.104:3001/api/WebApi/GetMunicipality/"+ vdelimited});
  axios.all([
    axios.get('http://10.11.27.104:3001/api/WebApi/GetMunicipality/'+vselectedprov )
  ])
  .then(axios.spread((data2) => {
    this.setState({
       municipalityTemp2List: data2.data,  
    })}
    ))  
  
 // this.setState({ municipalityTemp2List: this.state.municipalityList });
  console.log(this.state.municipalityTempList);
}

  birthdateChange = date => {
    let state = this.state;
    let value = moment(date).format('MM/DD/YYYY');
    this.setState({
      ...state,
      fields: { ...state.fields, "birthdate": value }
    });
    console.log(this.state.fields);
  }

  filesOnChange = sender => {
    let files = sender.target.files;
    let state = this.state;
    this.setState({
      ...state,
      files: files
    });
  }

  onAddressChange = e => {
    if (e.target.value == "Y") {
      
      axios.all([
        axios.get('http://10.11.27.104:3001/api/WebApi/GetMunicipality/'+this.state.fields.provincepres )
      ])
      .then(axios.spread((data2) => {
        this.setState({
           municipalityTemp2List: data2.data,  
        })}
        ))  
      
      let state = this.state;
      this.setState({
        fields: {
          ...state.fields, "streetperm": state.fields.streetpres, "cityperm": state.fields.citypres
          , "provinceperm": state.fields.provincepres, "countryperm": state.fields.countrypres, "zipcodeperm": state.fields.zipcodepres
        }
      });
    } else {
     this.setState({vConfirmadd: "N"});
    }
  };

  loadProduct = event => {
    this.props.history.push({ pathname: '/productinfo', state: this.state.fields })
  }

  selectCountry(val) {
    this.setState({ country: val });
  }

  selectMunicipality(val) {
      this.setState({ municipality: val });
    }

    selectProvince(val) {
        this.setState({ province: val });
    }

  selectRegion(val) {
    this.setState({ region: val });
  }

  

  //Function which is called when the component loads for the first time

  async componentDidMount() {
    axios.all([
      axios.get('http://10.11.27.104:3001/api/WebApi/GetCountry'    ),
      axios.get('http://10.11.27.104:3001/api/WebApi/GetMunicipality/PH-ABR' ), 
      axios.get('http://10.11.27.104:3001/api/WebApi/GetProvince'),
      axios.get('http://10.11.27.104:3001/api/WebApi/GetIncomeSource'),
      axios.get('http://10.11.27.104:3001/api/WebApi/GetProfession'),
    ])
    .then(axios.spread((data1,data2,data3,data4,data5) => {
      this.setState({
        countriesList: data1.data,
        municipalityList: data2.data, municipalityTempList: data2.data,  municipalityTemp2List: data2.data,
        provinceList: data3.data,
        incomesourceList: data4.data,
            professionList: data5.data
        })
        this.setState({
            countrypres: "PH"
        }); 

    }
      ))}



  render() {
    const { options, vFatka, vConfirmadd, values } = this.state;
    console.log(this.state);
    const { province, municipality, country, region } = this.state;


    if (!this.state.countriesList)
      return (<p>Loading data</p>)
    return (
      <div >
        {/* 1st Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" >
            <Heading size={600} marginLeft={12} >Personal Information</Heading>
          </Pane>
        </Pane>

        {/* 2nd Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="30%"
              marginRight={10}>
              <TextInput
                name="firstname"
                placeholder="First Name"
                width="100%"
                required
                validationMessage="This field is required"
                value={this.state.fields.firstname}
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
            <Pane width="30%"
              marginRight={10}>
              <TextInput
                name="middlename"
                placeholder="Middle Name"
                width="100%"
                required
                validationMessage="This field is required"
                value={this.state.middlename}
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
            <Pane width="30%"
              marginRight={10}>
              <TextInput
                name="lastname"
                placeholder="Last Name, Suffix"
                width="100%"
                required
                validationMessage="This field is required"
                value={this.state.lastname}
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>

        {/* 3rd Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="30%"
              marginRight={10}>
              <SelectField
                name="nationality"
                description="Nationality"
                width="100%"
                value={this.state.nationality}
                onChange={this.fieldOnChange}
              >
                <option value="PH" selected>Filipino</option>
                <option value="US" >Foreign</option>
              </SelectField>
            </Pane>
            <Pane width="30%"
              marginRight={10}>
              <Text marginRight={15}>BirthDate:</Text>
              <DatePicker format='MM/DD/YYYY'
                onChange={this.birthdateChange}
              >
              </DatePicker>
            </Pane>
            <Pane width="30%"
              marginRight={10}>
              <SelectField
                name="placeofbirth"
                value={this.state.fields.placeofbirth}
                width="100%"
                onChange={this.fieldOnChange}
                description="Place of Birth"
              >
                {this.state.provinceList.data.map((vprovince) => {
                 return <option value={vprovince.code}    > {vprovince.name} </option>
                })}
              </SelectField>
            </Pane>
          </Pane>
        </Pane>

        {/* 4th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="50%"
              marginRight={10}>

          <Text marginRight={15}>Are you a US Person?</Text>
              <Radio.Group onChange={this.fieldOnChange } defaultValue="N"  
               name="fatcadeclaration">
              <Radio value="Y">Yes</Radio> <Radio value="N">No</Radio>
                </Radio.Group>
            </Pane>
          </Pane>
        </Pane>

        {/* 5th Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" >
            <Heading size={600} marginLeft={12} >Contact Information</Heading>
          </Pane>
        </Pane>

        {/* 6th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="40%"
              marginRight={10}>
              <TextInput
                name="mobilenumber"
                placeholder="Mobile Number"
                width="100%"
                required
                validationMessage="This field is required"
                value={this.state.mobilenumber}
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
            <Pane width="40%"
              marginRight={10}>
              <TextInput
                name="email"
                placeholder="E-mail"
                width="100%"
                required
                validationMessage="This field is required"
                value={this.state.fields.email}
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>

        {/* 7th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="81%"
              marginRight={10}>
              <TextInput
                name="streetpres"
                value={this.state.fields.streetpres}
                placeholder="Present Address (House / Street / Barangay)"
                width="100%"
                required
                validationMessage="This field is required"
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>


        {/* 8th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
        
          <Pane display="flex" width="100%" marginLeft={12}>
          <Pane width="20%"
              marginRight={10}>

                        <SelectField
                            name="provincepres"
                            value={this.state.fields.provincepres}
                            width="100%"
                            onChange={this.fieldProvinceChange.bind(this)}
                            description="Province"
                        >
                            {this.state.provinceList.data.map((vprovince) => {
                                return <option value={vprovince.code}> {vprovince.name} </option>
                            })}
                        </SelectField>

            </Pane>   
            <Pane width="20%"
              marginRight={10}>
      

              <SelectField
                                name="citypres"
                                value={this.state.fields.citypres}
                                width="100%"
                                onChange={this.fieldOnChange}
                                description="City/Municipality"
                            > 
                                                     
                            {this.state.municipalityTempList.data.map((vmuni) => {
                                return <option value={vmuni.code}> {vmuni.name} </option>
                            })}                               
                            </SelectField>           
                            
            </Pane> 
       

            <Pane width="20%"
              marginRight={10}>
              <SelectField
                name="countrypres"
                value={this.state.fields.countrypres}
                width="100%"
                onChange={this.fieldOnChange}
                description="Country"
                
              >
                {this.state.countriesList.data.map((vcountries) => {
                                return <option value={vcountries.alpha3} > {vcountries.name} </option>
                })}
               </SelectField>










            </Pane>
            <Pane width="18%"
              marginRight={10}>
              <TextInput
                name="zipcodepres"
                value={this.state.fields.zipcodepres}
                placeholder="Zip Code"
                width="100%"
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>


        {/* 9th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="50%"
              marginRight={10}>

            <Text marginRight={15}>Is this your permanent address?</Text>
              <Radio.Group onChange={this.onAddressChange} defaultValue="N">
                <Radio value="Y">Yes</Radio> <Radio value="N">No</Radio> </Radio.Group>

            </Pane>
          </Pane>
        </Pane>

        {/* 10th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="81%"
              marginRight={10}>
              <TextInput
                name="streetperm"
                value={this.state.fields.streetperm}
                placeholder="Permanent Address (House / Street / Barangay)"
                width="100%"
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>


        {/* 11th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
          <Pane width="20%"
              marginRight={10}>


                        <SelectField
                            name="provinceperm"
                            value={this.state.fields.provinceperm}
                            width="100%"
                            onChange={this.fieldProvinceChange2.bind(this)}
                            description="Province"
                        >
                            {this.state.provinceList.data.map((vprovince) => {
                                return <option value={vprovince.code}> {vprovince.name} </option>
                            })}
                        </SelectField>



            </Pane>   
            <Pane width="20%"
              marginRight={10}>


              <SelectField
                                name="cityperm"
                                value={this.state.fields.cityperm}
                                width="100%"
                                onChange={this.fieldOnChange}
                                description="City/Municipality"
                            > 
                                                     
                            {this.state.municipalityTemp2List.data.map((vmuni2) => {
                                return <option value={vmuni2.code}> {vmuni2.name} </option>
                            })}                               
                            </SelectField>             
            </Pane> 

            <Pane width="20%"
              marginRight={10}>
              <SelectField
                name="countryperm"
                value={this.state.fields.countryperm}
                width="100%"
                onChange={this.fieldOnChange}
                description="Country"
              >
                {this.state.countriesList.data.map((vcountries) => {
                 // return <option value={vcountries.name} > {vcountries.name} </option>
                 return <option value={vcountries.alpha3} > {vcountries.name} </option>
                })}
              </SelectField>
            </Pane>
            <Pane width="18%"
              marginRight={10}>
              <TextInput
                name="zipcodeperm"
                value={this.state.fields.zipcodeperm}
                placeholder="Zip Code"
                width="100%"
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>

        {/* 12th Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" >
            <Heading size={600} marginLeft={12} >Financial  Information</Heading>
          </Pane>
        </Pane>

        {/* 13rd Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="50%"
              marginRight={10}>
              <SelectField
                            name="sourceincome"
                            value={this.state.fields.sourceincome}
                            width="100%"
                            onChange={this.fieldOnChange}
                            description="Source of Income"
                        >
                            {this.state.incomesourceList.data.map((vincomesource) => {
                                return <option value={vincomesource.code}> {vincomesource.description} </option>
                            })}
              </SelectField>




            </Pane>
            <Pane width="50%"
              marginRight={10}>
                        <SelectField
                            name="naturework"
                            value={this.state.fields.naturework}
                            width="100%"
                            onChange={this.fieldOnChange}
                            description="Nature of Work"
                        >
                            {this.state.professionList.data.map((vprofession) => {
                                return <option value={vprofession.code}> {vprofession.description} </option>
                            })}
                        </SelectField>

            </Pane>
          </Pane>
        </Pane>
        {/* 12th Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Button height={36} marginRight={16} appearance="primary" intent="danger" onClick={this.loadProduct}>Next</Button>
          </Pane>
        </Pane>
      </div>)
  }
}











