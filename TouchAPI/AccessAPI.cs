﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using RestSharp;
using Newtonsoft.Json.Serialization;


namespace CoreOps.TouchAPI
{
    public class JsonContent : StringContent
    {
        public JsonContent(object obj) :
            base(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json")
        { }
    }

    public class AccessAPI
    {

        public static async Task<string> RequestTokenToAuthorizationServer()
        {
            Uri uriAuthorizationServer = new Uri("http://psbapiadev:8280/token");
            string clientId = "nkGQjxKSLU3W9J1jfNQ190My7jIa";// "Rh85w937GWrRE_3Nk95P9bfaePMa";
            string clientSecret = "FzXmQLje4Ix9_lGJeMEsuWyihOwa"; // "JIEIVUE8PSoO1VS8hYeZDqyxWdwa";
            string scope = "scope.readaccess";

            HttpResponseMessage responseMessage;
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage tokenRequest = new HttpRequestMessage(HttpMethod.Post, uriAuthorizationServer);
                HttpContent httpContent = new FormUrlEncodedContent(
                    new[]
                    {
                    new KeyValuePair<string, string>("grant_type", "client_credentials"),
                    new KeyValuePair<string, string>("client_id", clientId),
                    new KeyValuePair<string, string>("scope", scope),
                    new KeyValuePair<string, string>("client_secret", clientSecret)
                    });
                tokenRequest.Content = httpContent;
                responseMessage = await client.SendAsync(tokenRequest);
            }
            return await responseMessage.Content.ReadAsStringAsync();
        }

        public static async Task<string> RequestKeycloakToAuthorizationServer()
        {
            Uri uriAuthorizationServer = new Uri("http://psbcpegadev:8080/auth");
            string clientId = "P1DEV";
            string clientSecret = "7212a824-5dd9-414c-a464-aaca320f6324";
            string scope = "openid profile";
            string realm = "PSB-TEST-REALM";

            HttpResponseMessage responseMessage;
            using (HttpClient client = new HttpClient())
            {
                HttpRequestMessage tokenRequest = new HttpRequestMessage(HttpMethod.Post, uriAuthorizationServer);
                HttpContent httpContent = new FormUrlEncodedContent(
                    new[]
                    {
                    new KeyValuePair<string, string>("grant_type", "client_credentials"),
                    new KeyValuePair<string, string>("client_id", clientId),
                    new KeyValuePair<string, string>("scope", scope),
                    new KeyValuePair<string, string>("client_secret", clientSecret),
                    new KeyValuePair<string, string>("realm", realm)
                    });
                tokenRequest.Content = httpContent;
                responseMessage = await client.SendAsync(tokenRequest);
            }
            return await responseMessage.Content.ReadAsStringAsync();
        }

        public static async Task<string> GetWebApi(string rawJwtToken, string Url)
        {

            //string rawJwtKeycloak = RequestKeycloakToAuthorizationServer()
            //    .GetAwaiter()
            //    .GetResult();

            //string rawJwtToken = RequestTokenToAuthorizationServer()
            //    .GetAwaiter()
            //    .GetResult();

            AccessAPI.AuthorizationServerAnswer authorizationServerToken;
            authorizationServerToken = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthorizationServerAnswer>(rawJwtToken);

            HttpResponseMessage responseMessage;
            //using (HttpClient httpClient = new HttpClient())
            //{
            //    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authorizationServerToken.access_token);
            //    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, Url );
            //    responseMessage = await httpClient.SendAsync(request);
            //}

            using (HttpClientHandler httpClientHandler = new HttpClientHandler())
            {
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
                using (HttpClient httpClient = new HttpClient(httpClientHandler))
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authorizationServerToken.access_token);
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, Url);
                    responseMessage = await httpClient.SendAsync(request);

                }
            }


            return await responseMessage.Content.ReadAsStringAsync();
        }


        public static async Task<string> PostWebApi(string rawJwtToken, string Url, object value)
        {

            AccessAPI.AuthorizationServerAnswer authorizationServerToken;

            authorizationServerToken = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthorizationServerAnswer>(rawJwtToken);

            HttpResponseMessage responseMessage;
            //using (HttpClient httpClient = new HttpClient())
            //{
            //    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authorizationServerToken.access_token);
            //    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, Url);
            //    responseMessage = await httpClient.PostAsync(Url, new JsonContent(value));
            //    // string content = await responseMessage.Content.ReadAsStringAsync();
            //    //   responseMessage.EnsureSuccessStatusCode();
            //}


            using (HttpClientHandler httpClientHandler = new HttpClientHandler())
            {
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
                using (HttpClient httpClient = new HttpClient(httpClientHandler))
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authorizationServerToken.access_token);
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, Url);
                    responseMessage = await httpClient.PostAsync(Url, new JsonContent(value));
                    // string content = await responseMessage.Content.ReadAsStringAsync();
                    //   responseMessage.EnsureSuccessStatusCode();
                }
            }

            return await responseMessage.Content.ReadAsStringAsync();
            //   return await Task.Run(() = &gt; JsonObject.Parse(content));
        }


        public static async Task<Object> GetNoTokenApi(string Url)
        {            
            RestClient client = new RestClient(Url);
            RestRequest request = new RestRequest(Method.GET);

            TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
            RestRequestAsyncHandle handle = client.ExecuteAsync(request, r => taskCompletion.SetResult(r));
            RestResponse response = (RestResponse)(await taskCompletion.Task);

            return JsonConvert.DeserializeObject<Object>(response.Content);
        }

        public static async Task<string> PostNoTokenApi(string Url, object value)
        {
            HttpResponseMessage responseMessage;
            using (HttpClient httpClient = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, Url);
                responseMessage = await httpClient.PostAsync(Url, new JsonContent(value));
                // string content = await responseMessage.Content.ReadAsStringAsync();
                //   responseMessage.EnsureSuccessStatusCode();
            }

            return await responseMessage.Content.ReadAsStringAsync();
            //   return await Task.Run(() = &gt; JsonObject.Parse(content));
        }



        private class AuthorizationServerAnswer
        {
            public string access_token { get; set; }
            public string expires_in { get; set; }
            public string token_type { get; set; }

        }

    }
}
