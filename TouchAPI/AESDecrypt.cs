﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text;

namespace CoreOps.TouchAPI

{
    public class AESDecrypt
    {
        public const string key = "PSBAPIKEY";
        public const string iv = "PSBAPIIV";

        public static string Encrypt(string input)
        {
            using (var aes = new AesCryptoServiceProvider())
            {
                Encoding iso = Encoding.GetEncoding("ISO-8859-1");
                byte[] inputBytes = iso.GetBytes(input);

                Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(key, Encoding.UTF8.GetBytes(iv));
                aes.Key = pbkdf2.GetBytes(32);
                aes.IV = pbkdf2.GetBytes(16);
                aes.Mode = CipherMode.CBC;

                byte[] encrypted = aes.CreateEncryptor().TransformFinalBlock(inputBytes, 0, inputBytes.Length);

                return Convert.ToBase64String(encrypted);
            }
        }


        public static string Decrypt(string input)
        {
            using (var aes = new AesCryptoServiceProvider())
            {
                Encoding iso = Encoding.GetEncoding("ISO-8859-1");
                byte[] encodedBytes = Convert.FromBase64String(input); //iso.GetBytes(input2);

                Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(key, Encoding.UTF8.GetBytes(iv));
                aes.Key = pbkdf2.GetBytes(32);
                aes.IV = pbkdf2.GetBytes(16);
                aes.Mode = CipherMode.CBC;

                var decrypted = aes.CreateDecryptor().TransformFinalBlock(encodedBytes, 0, encodedBytes.Length);

                return Encoding.Default.GetString(decrypted);
            }
        }

    }
}
