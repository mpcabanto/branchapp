﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreOps.Controllers
{
    public class MessageBody
    {
        public string ApplicationNumber { get; set; }
        public string AddressPres { get; set; }
        public string ZipCodePres { get; set; }
        public string CityPres { get; set; }
        public string ProvincePres { get; set; }
        public string CountryPres { get; set; }
        public string AddressPerm { get; set; }
        public string ZipCodePerm { get; set; }
        public string CityPerm { get; set; }
        public string ProvincePerm { get; set; }
        public string SourceIncome { get; set; }
        public string NatureWork { get; set; }
        public string ShortName { get; set; }
        public string Nationality { get; set; }
        public string CustomerCategory { get; set; }
        public string Gender { get; set; }
        public string CustCommnunicationMode { get; set; }
        public string Language { get; set; }
        public string PlaceOfBirth { get; set; }
        public string BirthCountry { get; set; }
        public string RiskLevel { get; set; }
        public string AccountNumber { get; set; }
        public string AccountClass { get; set; }
        public string AccountClassType { get; set; }
        public string AccountDescription { get; set; }
        public string AccountType { get; set; }
        public string Currency { get; set; }
        public string BranchCode { get; set; }
        public string MediaType { get; set; }
        public string Location { get; set; }
    }

    public class CreateAccountStatusResponse
    {
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
    }
}
