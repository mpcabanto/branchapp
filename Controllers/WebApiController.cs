﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using CoreOps.TouchAPI;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Cors;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CoreOps.Controllers
{
    //[DisableCors]

    //[EnableCors("CorsPolicy")]
    //[ApiController]
    public class WebApiController : Controller
    {

       //Start of List of Values
       // TO GET COUNTRY
       // GET: api/<controller>
        [HttpGet]
        [EnableCors("AnotherPolicy")]
        [Route("api/[controller]/GetCountry")]
                public String GetCountry(string vNotApplicable)
        {
            string url = "https://psbapiadev:8243/country/"; 
            string reqToken = AccessAPI.RequestTokenToAuthorizationServer()
                .GetAwaiter()
                .GetResult();
            string vresult = AccessAPI.GetWebApi(reqToken, url)
                .GetAwaiter()
                .GetResult();
            string vDecrypted = AESDecrypt.Decrypt(vresult);
            dynamic json = JsonConvert.DeserializeObject(vDecrypted);
            String jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(json);
            return jsonString;
        }

        // TO GET PROVINCE
        // GET: api/<controller>
        [HttpGet]
        [EnableCors("AnotherPolicy")]
        [Route("api/[controller]/GetProvince")]
        public String GetProvince(string vNotApplicable)
        {
            string url = "https://psbapiadev:8243/country/PH/province/";
            string reqToken = AccessAPI.RequestTokenToAuthorizationServer()
                .GetAwaiter()
                .GetResult();
            string vresult = AccessAPI.GetWebApi(reqToken, url)
                .GetAwaiter()
                .GetResult();
            string vDecrypted = AESDecrypt.Decrypt(vresult);
            dynamic json = JsonConvert.DeserializeObject(vDecrypted);
            String jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(json);
            return jsonString;
        }


        // TO GET MUNICIPALITY
        // GET: api/<controller>
        [HttpGet("{provcode}")]
        // [HttpGet("{country}")]
        [EnableCors("AnotherPolicy")]
        [Route("api/[controller]/GetMunicipality/{provcode}")]
        public String GetMunicipality([FromRoute]string provcode)
        {
           // string vDecryp1 = AESDecrypt.Encrypt(provcode);
            string url = "https://psbapiadev:8243/country/PH/town/" + provcode;
            string reqToken = AccessAPI.RequestTokenToAuthorizationServer()
                .GetAwaiter()
                .GetResult();
            string vresult = AccessAPI.GetWebApi(reqToken, url)
                .GetAwaiter()
                .GetResult();
            string vDecrypted = AESDecrypt.Decrypt(vresult);
            dynamic json = JsonConvert.DeserializeObject(vDecrypted);
            String jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(json);
            return jsonString;
        }

        // TO GET INCOME SOURCE
        // GET: api/<controller>
        [HttpGet]
        [EnableCors("AnotherPolicy")]
        [Route("api/[controller]/GetIncomeSource")]
        public String GetIncomeSource(string vNotApplicable)
        {
            string url = "https://psbapiadev:8243/form/income-source";
            string reqToken = AccessAPI.RequestTokenToAuthorizationServer()
                .GetAwaiter()
                .GetResult();
            string vresult = AccessAPI.GetWebApi(reqToken, url)
                .GetAwaiter()
                .GetResult();
            string vDecrypted = AESDecrypt.Decrypt(vresult);
            dynamic json = JsonConvert.DeserializeObject(vDecrypted);
            String jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(json);
            return jsonString;
        }


        // TO GET PROFESSION
        // GET: api/<controller>
        [HttpGet]
        [EnableCors("AnotherPolicy")]
        [Route("api/[controller]/GetProfession")]
        public String GetProfession(string vNotApplicable)
        {
            string url = "https://psbapiadev:8243/form/profession";
            string reqToken = AccessAPI.RequestTokenToAuthorizationServer()
                .GetAwaiter()
                .GetResult();
            string vresult = AccessAPI.GetWebApi(reqToken, url)
                .GetAwaiter()
                .GetResult();
            string vDecrypted = AESDecrypt.Decrypt(vresult);
            dynamic json = JsonConvert.DeserializeObject(vDecrypted);
            String jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(json);
            return jsonString;
        }

        //End of LOVs


        // Call Account Opening API
        // POST api/<controller>
        [HttpPost]
        [EnableCors("AnotherPolicy")]
        [Route("api/[controller]/CreateAccount")]
        public String Post([FromBody] JObject Jvalues)  //jen

        {
            string value = JsonConvert.SerializeObject(Jvalues).ToString();
            value = value.Replace(@"\", "");

            string vEncrypt = AESDecrypt.Encrypt(value);//++

            string url = "https://psbapiadev:8243/onboarding/applications";

            string reqToken = AccessAPI.RequestTokenToAuthorizationServer() //Request Token
                .GetAwaiter()
                .GetResult();

            string vresult = AccessAPI.PostWebApi(reqToken, url, vEncrypt)
              .GetAwaiter()
              .GetResult();

            string vDecrypted = AESDecrypt.Decrypt(vresult);
            dynamic json = JsonConvert.DeserializeObject(vDecrypted);
            String jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(json);
            return jsonString;
        }





        // Call Generic Post API
        // POST api/<controller>
        [HttpPost]
        [EnableCors("AnotherPolicy")]
        [Route("api/[controller]/CallAPI")]
        public String Post2([FromBody]  JObject data)  //jen

        {
            string vresult;
            string vDecrypted;
            String jsonString = "x";
            string APIurl;
            string APIdata;
            string APImethod;
            string value;


            GenericAPIPropDataString DeserializedDataString;
            GenericAPIPropDataJObject DeserializedDataJObject;

            try
            {
                DeserializedDataString = JsonConvert.DeserializeObject<GenericAPIPropDataString>(data.ToString(Newtonsoft.Json.Formatting.None));
                 APIurl = DeserializedDataString.APIurl;
                 APIdata = DeserializedDataString.APIdata;
                 APImethod = DeserializedDataString.APImethod;
            }
            catch (Exception e)
            {
                DeserializedDataJObject = JsonConvert.DeserializeObject<GenericAPIPropDataJObject>(data.ToString(Newtonsoft.Json.Formatting.None));
                 APIurl = DeserializedDataJObject.APIurl;
                 JObject APIdataJObject = DeserializedDataJObject.APIdata;
                APIdata = JsonConvert.SerializeObject(APIdataJObject).ToString();
                APImethod = DeserializedDataJObject.APImethod;
            }

           
            string vEncrypt;
            string Stringfyvalue;
            
            string reqToken = AccessAPI.RequestTokenToAuthorizationServer() //Request Token
                .GetAwaiter()
                .GetResult();

            if (APImethod == "GET")
            {
                try
                {
                    vresult = AccessAPI.GetWebApi(reqToken, APIurl + APIdata)
                                .GetAwaiter()
                                .GetResult();
                    vDecrypted = AESDecrypt.Decrypt(vresult);
                    dynamic json = JsonConvert.DeserializeObject(vDecrypted);
                    jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(json);

                    if (jsonString.Contains("error"))
                    {
                        Stringfyvalue = APIdata;
                        vEncrypt = AESDecrypt.Encrypt(Stringfyvalue);
                        vresult = AccessAPI.GetWebApi(reqToken, APIurl + vEncrypt)
                                    .GetAwaiter()
                                    .GetResult();
                        vDecrypted = AESDecrypt.Decrypt(vresult);
                        dynamic json2 = JsonConvert.DeserializeObject(vDecrypted);
                        jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(json2);
                    }

                }
                catch (Exception e)
                {
                    Stringfyvalue = APIdata;
                    vEncrypt = AESDecrypt.Encrypt(Stringfyvalue);
                    vresult = AccessAPI.GetWebApi(reqToken, APIurl + vEncrypt)
                                .GetAwaiter()
                                .GetResult();
                    vDecrypted = AESDecrypt.Decrypt(vresult);
                    dynamic json = JsonConvert.DeserializeObject(vDecrypted);
                    jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(json);
                }
                finally
                { }
            }
            else //Post
            {
                value = APIdata;
               // value = value.Replace(@"\", "");
                vEncrypt = AESDecrypt.Encrypt(value);//++

                vresult = AccessAPI.PostWebApi(reqToken, APIurl, vEncrypt)
                  .GetAwaiter()
                  .GetResult();

                 vDecrypted = AESDecrypt.Decrypt(vresult);
                dynamic jsonb = JsonConvert.DeserializeObject(vDecrypted);
                 jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(jsonb);

                if (jsonString.Contains("error"))
                {  //dito ako tumigil
                    value = APIdata;
                    value = value.Replace(@"\", "");
                    dynamic jsonf = JsonConvert.DeserializeObject(value);
                    vEncrypt = AESDecrypt.Encrypt(jsonf);//++

                    vresult = AccessAPI.PostWebApi(reqToken, APIurl, vEncrypt)
                      .GetAwaiter()
                      .GetResult();

                    vDecrypted = AESDecrypt.Decrypt(vresult);
                    dynamic jsonc = JsonConvert.DeserializeObject(vDecrypted);
                    jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(jsonc);
                } //dito ako tumigil
            }
            return jsonString;
        }







        // Get transactions from Kafka
        [HttpPost]
        [EnableCors("AnotherPolicy")]
        [Route("api/[controller]/GetApi")]
        public String GetApi([FromBody]  KafkaProp data)

        {
            string url = data.kafkaurl; // JsonConvert.SerializeObject(Jvalues).ToString();
            string reqToken = AccessAPI.RequestTokenToAuthorizationServer()
                .GetAwaiter()
                .GetResult();
            string vresult = AccessAPI.GetWebApi(reqToken, url)
                .GetAwaiter()
                .GetResult();
            string vDecrypted = AESDecrypt.Decrypt(vresult);
            dynamic json = JsonConvert.DeserializeObject(vDecrypted);
            String jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(json);
            return jsonString;
        }

        // Get transactions from Kafka
        [HttpPost]
        [EnableCors("AnotherPolicy")]
        [Route("api/[controller]/GetKafka")]
        public JsonResult GetKafka([FromBody]  KafkaProp data)

        {
            string kafkaurl = data.kafkaurl;
            Object vresult = AccessAPI.GetNoTokenApi(kafkaurl)
                .GetAwaiter()
                .GetResult();
            return Json(vresult);
        }

        // Post transactions from Kafka
        [HttpPost]
        [EnableCors("AnotherPolicy")]
        [Route("api/[controller]/PostKafka")]
        public JsonResult PostKafka([FromBody] KafkaProp data)
        // public JsonResult PostKafka([FromBody] string Jvalues, JObject data)

        {
            string kafkaurl = data.kafkaurl;
            JObject kafkadata = data.kafkadata;

            String vresult = AccessAPI.PostNoTokenApi(kafkaurl, kafkadata)
            //Object vresult = AccessAPI.GetNoTokenApi(value)
                .GetAwaiter()
                .GetResult();

            dynamic json = JsonConvert.DeserializeObject(vresult);
            return Json(json);
        }


        // Post transactions from Kafka
        [HttpPost]
        [EnableCors("AnotherPolicy")]
        [Route("api/[controller]/PostKafka2")]
        public String PostKafka2([FromBody] KafkaProp data)
        // public JsonResult PostKafka([FromBody] string Jvalues, JObject data)

        {
            string kafkaurl = data.kafkaurl;
            JObject kafkadata = data.kafkadata;

            string value = JsonConvert.SerializeObject(kafkadata).ToString();
            value = value.Replace(@"\", "");

            string vresult = AccessAPI.PostNoTokenApi(kafkaurl, kafkadata)
            //Object vresult = AccessAPI.GetNoTokenApi(value)
                .GetAwaiter()
                .GetResult();

            dynamic json = JsonConvert.DeserializeObject(vresult);
            String jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(json);
            return jsonString;
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        public class TokenProp
        {
            public string Token { get; set; }

        }

        public class KafkaProp
        {
            public string kafkaurl { get; set; }
            public JObject kafkadata { get; set; }

        }

        public class GenericAPIPropDataString
        {
            public string APIurl { get; set; }
            public string APIdata { get; set; }
            public string APImethod { get; set; }

        }

        public class GenericAPIPropDataJObject
        {
            public string APIurl { get; set; }
            public JObject APIdata { get; set; }
            public string APImethod { get; set; }

        }


    }
}
