









import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { Counter } from './components/Counter';
import { NACQueue } from './components/NACQueue';
import { Dashboard } from './components/Dashboard';
import { Secured } from './components/Secured';
import { ClientDetails } from './components/ClientDetails';
import { ProductInfo } from './components/ProductInfo';
import { EndForm } from './components/EndForm';

export default class App extends Component {
  displayName = App.name

  render() {
   
return (
      <Layout>

        <Route exact path='/' component={Home} />
        <Route path='/counter' component={Counter} />
        <Route path='/nacqueue' component={NACQueue} />
        <Route path='/dashboard' component={Dashboard} />
        <Route path='/secured' component={Secured} />     
        <Route path='/clientdetails' component={ClientDetails} />     
        <Route path='/productinfo' component={ProductInfo} />
        <Route path='/endform' component={EndForm} />    
      </Layout>
    ); 
  }
}
        



         {/*

import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { sample } from './components/sample';

export default class App extends Component {
  displayName = App.name

  render() {
   
return (
      <Layout>
        <Route exact path='/' component={sample} />
      </Layout>
    ); 
  }
}



 */}

