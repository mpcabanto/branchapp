import React, { Component } from 'react';
import Panel from 'react-bootstrap/lib/Panel'
import axios from 'axios';
import { Pane, Text, Heading, TextInput, Select, SelectField, RadioGroup, Combobox, Button, Switch,TextInputField } from 'evergreen-ui';
import { Link } from 'react-router-dom';
import 'antd/dist/antd.css';
import { Radio, DatePicker } from 'antd';
import moment from 'moment';

//This Component is a child Component of Customers Component
export class ClientDetails extends Component {
  displayName = ClientDetails.name


  
  constructor(props) {
    super(props);
    this.loadProduct = this.loadProduct.bind(this)
    this.state = {
      options: [
        { label: 'Yes', value: 'Y' },
        { label: 'No', value: 'N' }
      ],
      vFatka: 'N',
      vConfirmadd: 'N',
      vmunicipality : '', municipality: '',municipalityTempList: [],municipalityTemp2List: [], municipalityTemp3List: [],
      tempval: '',
      vmailaddress: 'PERM',
      fields: {
        firstname: '',
        middlename: '',
        lastname: '',
        birthdate: '',
        mobilenumber: '',
        nationality: 'FILIPINO',
        placeofbirth: 'PH-CAL',
        email: '',
        unitnopres: '',
        bldgpres: '',
        unitnoperm: '',
        bldgperm: '',
        cif: '00000000',
        userid: 'TESTER',
        customertype: '',
        gender: 'M',
        civilstatus: 'M',
        spousename: 'NOT APPLICABLE',
        employername: 'EMPLOYER NAME',
        employeraddress: 'EMPLOYER ADDRESS',
        monthlyincome: "0",
        documenttype: '',
        producttype: '',
        rccode: '111',
        accountfacilities: '',
        fatcadeclaration: '',
        applicationrefno: '',
        streetpres: "",
        barangaypres: "n/a",
        citypres: "2800",
        provincepres: "PH-CAL",
        countrypres: "AFG",
        zipcodepres: "",
        streetperm: "",
        barangayperm: "n/a",
        cityperm: "2800",
        provinceperm: "PH-CAL",
        countryperm: "AFG",
        zipcodeperm: "",
        provincemail: "PH-CAL",
        countrymail: "AFG",
        zipcodemail: "",        
        sourceincome: "0001",
        naturework: "0001"
      }
    }

    this.fieldProvinceChange = this.fieldProvinceChange.bind(this)
  }

  fieldOnChange = sender => {
    let fieldName = sender.target.name;
    let value = sender.target.value;
    let state = this.state;
    this.setState({
      ...state,
      fields: { ...state.fields, [fieldName]: value }
      ,
    });
    console.log(this.state.fields);
  }

fieldProvinceChange =sender => {
    let fieldName = sender.target.name;
    let value = sender.target.value;
    let state = this.state;
    let vselectedprov = value.substring(0,6)

    this.setState({
      ...state,
      fields: { ...state.fields, [fieldName]: value  ,"countrypres":"PH"}
    });
    const config = {
      mode:"no-cors",
      headers: {
        "Access-Control-Allow-Origin": "GET, POST",
        "Access-Control-Max-Age": "86400",
        "Access-Control-Allow-Headers":"Content-Type",
        "Content-Type": "application/x-www-form-urlencoded",
      //  "Accept": "application/json"
      }}
    axios.all([
        axios.get('https://10.11.27.104:3001/api/WebApi/GetMunicipality/' + vselectedprov,{config})
    ])
    .then(axios.spread((data2) => {
      this.setState({
         municipalityTempList: data2.data,  
      })}
        ))  

    console.log(this.state.municipalityTempList);
  }

fieldProvinceChange2 =sender => {
  let fieldName = sender.target.name;
  let value = sender.target.value;
  let state = this.state;
  let vselectedprov = value.substring(0,6)

  this.setState({
    ...state,
    fields: { ...state.fields, [fieldName]: value ,"countryperm":"PH"}
  });
  const config = {
    mode:"no-cors",
    headers: {
      "Access-Control-Allow-Origin": "GET, POST",
      "Access-Control-Max-Age": "86400",
      "Access-Control-Allow-Headers":"Content-Type",
      "Content-Type": "application/x-www-form-urlencoded",
    //  "Accept": "application/json"
    }}
  axios.all([
    axios.get('https://10.11.27.104:3001/api/WebApi/GetMunicipality/'+vselectedprov ,{config})
  ])
  .then(axios.spread((data2) => {
    this.setState({
       municipalityTemp2List: data2.data,  
    })}
    ))  

    this.setState({
        countryperm: "PH"
    }); 
}

fieldProvinceChange3 =sender => {
  let fieldName = sender.target.name;
  let value = sender.target.value;
  let state = this.state;
  let vselectedprov = value.substring(0,6)

  this.setState({
    ...state,
    fields: { ...state.fields, [fieldName]: value ,"countrymail":"PH"}
  });
  const config = {
    mode:"no-cors",
    headers: {
      "Access-Control-Allow-Origin": "GET, POST",
      "Access-Control-Max-Age": "86400",
      "Access-Control-Allow-Headers":"Content-Type",
      "Content-Type": "application/x-www-form-urlencoded",
    //  "Accept": "application/json"
    }}
  axios.all([
    axios.get('https://10.11.27.104:3001/api/WebApi/GetMunicipality/'+vselectedprov ,{config})
  ])
  .then(axios.spread((data3) => {
    this.setState({
       municipalityTemp3List: data3.data,  
    })}
    ))  

    this.setState({
        countrymail: "PH"
    }); 
}



MunicipalityChange = sender => {
  let value = sender.target.value;
  let state = this.state;
  this.setState({
    fields: {
      ...state.fields,  "citypres": value
      , "zipcodepres":value ,"countrypres":"PH"
    }
  });
  console.log(this.state.fields);
}

MunicipalityChange2 = sender => {
  let value = sender.target.value;
  let state = this.state;
  this.setState({
    fields: {
      ...state.fields,  "cityperm": value
      , "zipcodeperm":value
    }
  });
  console.log(this.state.fields);
}

MunicipalityChange3 = sender => {
  let value = sender.target.value;
  let state = this.state;
  this.setState({
    fields: {
      ...state.fields,  "citymail": value
      , "zipcodemail":value
    }
  });
  console.log(this.state.fields);
}



  birthdateChange = date => {
    let state = this.state;
    let value = moment(date).format('MM/DD/YYYY');
    this.setState({
      ...state,
      fields: { ...state.fields, "birthdate": value }
    });
    console.log(this.state.fields);
  }

  filesOnChange = sender => {
    let files = sender.target.files;
    let state = this.state;
    this.setState({
      ...state,
      files: files
    });
  }

  onAddressChange = e => {
    if (e.target.value == "Y") {
      const config = {
        mode:"no-cors",
        headers: {
          "Access-Control-Allow-Origin": "GET, POST",
          "Access-Control-Max-Age": "86400",
          "Access-Control-Allow-Headers":"Content-Type",
          "Content-Type": "application/x-www-form-urlencoded",
        //  "Accept": "application/json"
        }}
      axios.all([
        axios.get('https://10.11.27.104:3001/api/WebApi/GetMunicipality/'+this.state.fields.provincepres.substring(0,6),{config} )
      ])
      .then(axios.spread((data2) => {
        this.setState({
           municipalityTemp2List: data2.data,  
        })}
        ))  
      
      let state = this.state;
      this.setState({
        fields: {
          ...state.fields, "streetperm": state.fields.streetpres, "cityperm": state.fields.citypres
          , "provinceperm": state.fields.provincepres, "countryperm": state.fields.countrypres, "zipcodeperm": state.fields.zipcodepres
        }
      });
    } else {
     this.setState({vConfirmadd: "N"});
    }
  };

  loadProduct = event => {console.log(this.state.fields.firstname);
    if  (this.state.fields.firstname.trim() != "" && this.state.fields.middlename.trim() != ""  && this.state.fields.lastname.trim() != ""
    && this.state.fields.birthdate.trim() != "" && this.state.fields.mobilenumber.trim() != "" && this.state.fields.email.trim() != ""
    && this.state.fields.streetpres.trim() != "" && this.state.fields.zipcodepres.trim()     && this.state.fields.streetperm.trim() != "" && this.state.fields.zipcodeperm.trim()  
  ) 
    {this.props.history.push({ pathname: '/productinfo', state: this.state.fields })}
    else
    { alert("Please fill out all required fields.");}

  }


  selectCountry(val) {
    this.setState({ country: val });
  }

  selectMunicipality(val) {
      this.setState({ municipality: val });
    }

    selectProvince(val) {
        this.setState({ province: val });
    }

  selectRegion(val) {
    this.setState({ region: val });
  }

  

  








  onAddressChange = e => {
    if (e.target.value == "Y") {
      const config = {
        mode:"no-cors",
        headers: {
          "Access-Control-Allow-Origin": "GET, POST",
          "Access-Control-Max-Age": "86400",
          "Access-Control-Allow-Headers":"Content-Type",
          "Content-Type": "application/x-www-form-urlencoded",
        //  "Accept": "application/json"
        }}
      axios.all([
        axios.get('https://10.11.27.104:3001/api/WebApi/GetMunicipality/'+this.state.fields.provincepres.substring(0,6),{config} )
      ])
      .then(axios.spread((data2) => {
        this.setState({
           municipalityTemp2List: data2.data,  
        })}
        ))  
      
      let state = this.state;
      this.setState({
        fields: {
          ...state.fields, "streetperm": state.fields.streetpres, "cityperm": state.fields.citypres
          , "provinceperm": state.fields.provincepres, "countryperm": state.fields.countrypres, "zipcodeperm": state.fields.zipcodepres
        }
      });
    } else {
     this.setState({vConfirmadd: "N"});
    }
  };














  onMailAddressTagChange = e => {
    let value = e.target.value;
    this.setState({vmailaddress: e.target.value});
  }






    renderMailAddress() {
    const { classes } = this.props;
    const { clientInfo } = this.state;
    if (this.state.vmailaddress == "OTH") {
        return (
          <div>
        {/* 10th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="81%"
              marginRight={10}>
              <TextInput
                name="streetmail"
                value={this.state.fields.streetmail}
                placeholder="Mailing Address (House / Street / Barangay)"
                width="100%"
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>



        {/* 11th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
          <Pane width="20%"
              marginRight={10}>

                        <SelectField
                            name="provincemail"
                            value={this.state.fields.provincemail}
                            width="100%"
                            onChange={this.fieldProvinceChange3.bind(this)}
                            description="Province"
                        >
                            {this.state.provinceList.data.map((vprovince) => {
                                return <option value={vprovince.code }> {vprovince.name} </option>
                            })}
                        </SelectField>

            </Pane>   
            <Pane width="20%"
              marginRight={10}>


              <SelectField
                                name="citymail"
                                value={this.state.fields.citymail}
                                width="100%"
                                onChange={this.MunicipalityChange3}
                                description="City/Municipality"
                            > 
                                                     
                            {this.state.municipalityTemp3List.data.map((vmuni3) => {
                                return <option value={vmuni3.code }> {vmuni3.name} </option>
                            })}                               
                            </SelectField>             
            </Pane> 

            <Pane width="20%"
              marginRight={10}>
              <SelectField
                name="countrymail"
                value={this.state.fields.countrymail}
                width="100%"
                onChange={this.fieldOnChange}
                description="Country"
              >
                {this.state.countriesList.data.map((vcountries) => {
                 // return <option value={vcountries.name} > {vcountries.name} </option>
                 return <option value={vcountries.alpha2} > {vcountries.name} </option>
                })}
              </SelectField>
            </Pane>
            <Pane width="18%"
              marginRight={10}>
              <Text size={301} marginLeft={1} >Zip Code</Text>
              <TextInput
                name="zipcodemail"
                value={this.state.fields.zipcodemail}
                placeholder="Zip Code"
                width="100%"
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>
      </div>

        );
    }
    else {
      
     }
}
































  //Function which is called when the component loads for the first time

 componentDidMount () {

  this.getAPI();
    }

    getAPI= async() => {

      const headers = {"headers":"Access-Control-Allow-Origin"}
       

        await axios.all([
          axios.get('https://10.11.27.104:3001/api/WebApi/GetCountry',{headers}),
          axios.get('https://10.11.27.104:3001/api/WebApi/GetMunicipality/PH-ABR',{headers}),
          axios.get('https://10.11.27.104:3001/api/WebApi/GetProvince',{headers}),
          axios.get('https://10.11.27.104:3001/api/WebApi/GetIncomeSource',{headers}),
          axios.get('https://10.11.27.104:3001/api/WebApi/GetProfession',{headers}),
          //  axios.get('http://10.11.27.104:3001/api/WebApi/GetCountry',config),
          //  axios.get('http://10.11.27.104:3001/api/WebApi/GetMunicipality/PH-ABR',config),
          //  axios.get('http://10.11.27.104:3001/api/WebApi/GetProvince',config),
          //  axios.get('http://10.11.27.104:3001/api/WebApi/GetIncomeSource',config),
          //  axios.get('http://10.11.27.104:3001/api/WebApi/GetProfession',config),
        ])
        .then(axios.spread((data1,data2,data3,data4,data5) => {
          this.setState({
            countriesList: data1.data,
              countrypres: "PH",
            municipalityList: data2.data, municipalityTempList: data2.data, municipalityTemp2List: data2.data, municipalityTemp3List: data3.data,
            provinceList: data3.data,
            incomesourceList: data4.data,
            professionList: data5.data
            })   
        }
          ))

    let state = this.state;
      this.setState({
        ...state,
        fields: { ...state.fields  ,"countrypres":"PH","countryperm":"PH","countrymail":"PH"}
      });
    }

  render() {
    const { options, vFatka, vConfirmadd, values } = this.state;
    
    console.log(this.state);
    const { province, municipality, country, region } = this.state;


    if (!this.state.countriesList)
      return (<p>Loading data</p>)
    return (
      <div >
        {/* 1st Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" >
            <Heading size={600} marginLeft={12} >Personal Information</Heading>
          </Pane>
        </Pane>

        {/* 2nd Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="30%"
              marginRight={10}>
              <TextInput
                name="firstname"
                placeholder="First Name"
                width="100%"
                required
                validationMessage="This field is required"
                value={this.state.fields.firstname}
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
            <Pane width="30%"
              marginRight={10}>
              <TextInput
                name="middlename"
                placeholder="Middle Name"
                width="100%"
                required
                validationMessage="This field is required"
                value={this.state.middlename}
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
            <Pane width="30%"
              marginRight={10}>
              <TextInput
                name="lastname"
                placeholder="Last Name, Suffix"
                width="100%"
                required
                validationMessage="This field is required"
                value={this.state.lastname}
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>

        {/* 3rd Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="30%"
              marginRight={10}>
              <Text size={301} marginLeft={1} >Nationality</Text>
              <SelectField
                name="nationality"
                //description="Nationality"
                width="100%"
                value={this.state.nationality}
                onChange={this.fieldOnChange}
              >
                <option value="PH" selected>Filipino</option>
                <option value="US" >Foreign</option>
              </SelectField>
            </Pane>
            <Pane width="30%"
              marginRight={10}>
              <Text size={301} marginLeft={1} >Birth Date           </Text>
             {/*<Text marginRight={15}>BirthDate:</Text> */}
              <DatePicker format='MM/DD/YYYY'
                onChange={this.birthdateChange}
              >
              </DatePicker>
            </Pane>
            <Pane width="30%"
              marginRight={10}>
              <SelectField
                name="placeofbirth"
                value={this.state.fields.placeofbirth}
                width="100%"
                onChange={this.fieldOnChange}
                description="Place of Birth"
              >
                {this.state.provinceList.data.map((vprovince) => {
                 return <option value={vprovince.code}    > {vprovince.name} </option>
                })}
              </SelectField>
            </Pane>
          </Pane>
        </Pane>

        {/* 4th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="50%"
              marginRight={10}>

          <Text marginRight={15}>Are you a US Person?</Text>
              <Radio.Group onChange={this.fieldOnChange } defaultValue="N"  
               name="fatcadeclaration">
              <Radio value="Y">Yes</Radio> <Radio value="N">No</Radio>
                </Radio.Group>
            </Pane>
          </Pane>
        </Pane>

        {/* 5th Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" >
            <Heading size={600} marginLeft={12} >Contact Information</Heading>
          </Pane>
        </Pane>

        {/* 6th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="40%"
              marginRight={10}>
              <TextInput
                name="mobilenumber"
                placeholder="Mobile Number"
                width="100%"
                required
                validationMessage="This field is required"
                value={this.state.mobilenumber}
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
            <Pane width="40%"
              marginRight={10}>
              <TextInput
                name="email"
                placeholder="E-mail"
                width="100%"
                required
                validationMessage="This field is required"
                value={this.state.fields.email}
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>

        {/* 7th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="81%"
              marginRight={10}>
              <TextInput
                name="streetpres"
                value={this.state.fields.streetpres}
                placeholder="Present Address (House / Street / Barangay)"
                width="100%"
                required
                validationMessage="This field is required"
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>


        {/* 8th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
        
          <Pane display="flex" width="100%" marginLeft={12}>
          <Pane width="20%"
              marginRight={10}>

                        <SelectField
                            name="provincepres"
                            value={this.state.fields.provincepres}
                            width="100%"
                            onChange={this.fieldProvinceChange.bind(this)}
                            description="Province"
                        >
                            {this.state.provinceList.data.map((vprovince) => {
                                return <option value={vprovince.code}> {vprovince.name} </option>
                            })}
                        </SelectField>

            </Pane>   
            <Pane width="20%"
              marginRight={10}>
      

              <SelectField
                                name="citypres"
                                value={this.state.fields.citypres}
                                width="100%"
                                onChange={this.MunicipalityChange}
                                description="City/Municipality"
                            > 
                                                     
                            {this.state.municipalityTempList.data.map((vmuni) => {
                                return <option value={vmuni.code}> {vmuni.name} </option>
                            })}                               
                            </SelectField>           
                            
            </Pane> 
       

            <Pane width="20%"
              marginRight={10}>
              <SelectField
                name="countrypres"
                value={this.state.fields.countrypres}
                width="100%"
                onChange={this.fieldOnChange}
                description="Country"
                
                
              >
                {this.state.countriesList.data.map((vcountries) => {
                                return <option value={vcountries.alpha2} > {vcountries.name} </option>  
                })}
               
               </SelectField>


            </Pane>
            <Pane width="18%"
              marginRight={10}>
              <Text size={301} marginLeft={1} >Zip Code</Text>
              <TextInputField
                //label="tae"
                name="zipcodepres"
                value={this.state.fields.zipcodepres}
                placeholder="Zip Code"
                width="100%"
                onChange={this.fieldOnChange}
              >
              </TextInputField>
            </Pane>
          </Pane>
        </Pane>

        {/* 9th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="50%"
              marginRight={10}>

            <Text marginRight={15}>Is this your permanent address?</Text>
              <Radio.Group onChange={this.onAddressChange} defaultValue="N">
                <Radio value="Y">Yes</Radio> <Radio value="N">No</Radio> </Radio.Group>

            </Pane>
          </Pane>
        </Pane>

        {/* 10th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="81%"
              marginRight={10}>
              <TextInput
                name="streetperm"
                value={this.state.fields.streetperm}
                placeholder="Permanent Address (House / Street / Barangay)"
                width="100%"
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>


        {/* 11th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
          <Pane width="20%"
              marginRight={10}>

                        <SelectField
                            name="provinceperm"
                            value={this.state.fields.provinceperm}
                            width="100%"
                            onChange={this.fieldProvinceChange2.bind(this)}
                            description="Province"
                        >
                            {this.state.provinceList.data.map((vprovince) => {
                                return <option value={vprovince.code }> {vprovince.name} </option>
                            })}
                        </SelectField>

            </Pane>   
            <Pane width="20%"
              marginRight={10}>


              <SelectField
                                name="cityperm"
                                value={this.state.fields.cityperm}
                                width="100%"
                                onChange={this.MunicipalityChange2}
                                description="City/Municipality"
                            > 
                                                     
                            {this.state.municipalityTemp2List.data.map((vmuni2) => {
                                return <option value={vmuni2.code }> {vmuni2.name} </option>
                            })}                               
                            </SelectField>             
            </Pane> 

            <Pane width="20%"
              marginRight={10}>
              <SelectField
                name="countryperm"
                value={this.state.fields.countryperm}
                width="100%"
                onChange={this.fieldOnChange}
                description="Country"
              >
                {this.state.countriesList.data.map((vcountries) => {
                 // return <option value={vcountries.name} > {vcountries.name} </option>
                 return <option value={vcountries.alpha2} > {vcountries.name} </option>
                })}
              </SelectField>
            </Pane>
            <Pane width="18%"
              marginRight={10}>
              <Text size={301} marginLeft={1} >Zip Code</Text>
              <TextInput
                name="zipcodeperm"
                value={this.state.fields.zipcodeperm}
                placeholder="Zip Code"
                width="100%"
                onChange={this.fieldOnChange}
              >
              </TextInput>
            </Pane>
          </Pane>
        </Pane>








        {/* 9th Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="100%"
              marginRight={10}>

            <Text marginRight={15}>Please select your preferred mailing address:</Text>
              <Radio.Group onChange={this.onMailAddressTagChange} defaultValue="PERM">
                <Radio value="PRES">Present</Radio> 
                <Radio value="PERM">Permanent</Radio> 
                <Radio value="OTH">Others (Please enter mailing address below)</Radio></Radio.Group>
            </Pane>
          </Pane>
        </Pane>

        {this.renderMailAddress()}


        {/* 12th Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" >
            <Heading size={600} marginLeft={12} >Financial  Information</Heading>
          </Pane>
        </Pane>

        {/* 13rd Line */}
        <Pane display="flex" padding={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" marginLeft={12}>
            <Pane width="50%"
              marginRight={10}>
              <SelectField
                            name="sourceincome"
                            value={this.state.fields.sourceincome}
                            width="100%"
                            onChange={this.fieldOnChange}
                            description="Source of Income"
                        >
                            {this.state.incomesourceList.data.map((vincomesource) => {
                                return <option value={vincomesource.code }> {vincomesource.description} </option>
                            })}
              </SelectField>




            </Pane>
            <Pane width="50%"
              marginRight={10}>
                        <SelectField
                            name="naturework"
                            value={this.state.fields.naturework}
                            width="100%"
                            onChange={this.fieldOnChange}
                            description="Nature of Work"
                        >
                            {this.state.professionList.data.map((vprofession) => {
                                return <option value={vprofession.code }> {vprofession.description} </option>
                            })}
                        </SelectField>

            </Pane>
          </Pane>
        </Pane>
        {/* 12th Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Button height={36} marginRight={16} appearance="primary" intent="danger" onClick={this.loadProduct}>Next</Button>
          </Pane>
        </Pane>
      </div>)
  }
}











