import React, { Component } from 'react';
import {Tabs, Tab} from 'react-bootstrap-tabs'; 
import './Step.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { InputGroup, InputGroupButton, Glyphicon, Button, FormControl } from 'react-bootstrap';

import ItemModal from './ItemModal';

export class Dashboard extends Component {
    displayName = Dashboard.name
    constructor(props) {
        super(props)

        this.state = {
            items: [
                { id: 1, name: 'Jimmy', age: 26, showModal: false },
                { id: 2, name: 'Gede', age: 26, showModal: false },
                { id: 3, name: 'Mayu', age: 22, showModal: false },
                { id: 4, name: 'Ketut', age: 25, showModal: false },
            ],
        }
        this.handleItemChange = this.handleItemChange.bind(this)
    }

    handleFormUpdate() {
        return e => {
            const field = e.target.name
            const { form } = this.state
            form[field] = e.target.value
            this.setState({ form })
        }
    }

    handleModalHide() {
        return () => {
            let { items } = this.state
            items = items.map(item => ({
                ...item,
                showModal: false,
            }))
            this.setState({ items })
        }
    }

    handleModalShow() {
        return e => {
            e.preventDefault();

            this.setState({ showModal: true });
        };
    }

    handleEditItem(selectedItem) {
        return e => {
            e.preventDefault()
            let { items } = this.state
            items = items.map(item => ({
                ...item,
                showModal: selectedItem.id === item.id,
            }));
            this.setState({ items });
        };
    }

    handleItemChange(itemId) {
        return e => {
            let { items } = this.state;
            items = items.map(item => {
                if (item.id === itemId) {
                    item[e.target.name] = e.target.value
                }
                return item
            })
            this.setState({ items })
        }
    }

    render() {
        const { items } = this.state
        return (
            <div>
                <table className="table">
                    <tbody>
                        {items.map((item, index) => (
                            <tr key={index}>
                                <td>{index + 1}</td>
                                <td>{item.name}</td>
                                <td>{item.age}</td>
                                <td>
                                    <a
                                        className="btn btn-primary"
                                        onClick={this.handleEditItem(item)}
                                    >
                                        Edit
                  </a>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                {items.map((item, index) => (
                    <ItemModal
                        key={item.id}
                        show={item.showModal}
                        onHide={this.handleModalHide()}
                        onItemChange={this.handleItemChange}
                        item={item}
                    />
                ))}
            </div>
        );
    }

}
