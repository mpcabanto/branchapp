import React, { Component } from 'react';
import { NavHeader } from './NavHeader';
import { Header } from './Header';
import psblogo from './PSBLogo.png';
import './App.css';
import Clock from 'react-live-clock';


export class Layout extends Component {
  displayName = Layout.name

  render() {
    return (

      <div className="container">
        <div id="psblogo">
          <header className="PSB-header">
            <img src={psblogo} alt="logo" height="80" />

            <div className="clock">
              <label >Hello Guest! </label> <br />
              <Clock format={'dddd, MMMM DD, YYYY, h:mm:ss A'}
                ticking={true}
                timezone={'HongKong'} /> <br />
            </div>
          </header>
        </div>
        <div >
          <NavHeader />
        </div>
        <div className="body">
          <section >
            {this.props.children}
          </section>
        </div>
        <div className="footer-wrap2">
          <div className="member">CentralOps © 2019. Philippine Savings Bank.</div>
        </div>
      </div>
    );
  }
}
