import React, { Component, Fragment } from 'react';
import Secured from './Secured';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Pane, Text, Heading, TextInput, Select, SelectField, RadioGroup, Combobox, Button } from 'evergreen-ui';

export class Home extends Component {
  displayName = Home.name

  render() {
    return (
        <div >
            <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1} height={200} >
          <Pane display="flex" width="100%"  alignItems="center" justifyContent="center">
            <Heading size={600} marginLeft={20} >Welcome to PSBANK - Onboarding!</Heading>
          </Pane>
        </Pane>

                <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} >
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center"> <Link to="/clientdetails">
            <Button height={36} marginRight={16} appearance="primary" >Start</Button> </Link>
          </Pane>
        </Pane>
            </div>
    );
  }
}
