import React, { Component } from 'react';
import header from './Header.jpg';
import './App.css';


export class NavHeader extends Component {
  render() {
    return (
        <div>
        <header className="App-header">
          <img src={header} className="App-logo" alt="logo" />
        </header>
      </div>
    );
  }
}

