import React, { Component } from 'react';
import Panel from 'react-bootstrap/lib/Panel'
import axios from 'axios';
import { Pane, Text, Heading, TextInput, Select, SelectField, RadioGroup, Combobox, Button, Switch,TextInputField } from 'evergreen-ui';
import { Link } from 'react-router-dom';
import 'antd/dist/antd.css';
import { Radio, DatePicker } from 'antd';
import moment from 'moment';



//This Component is a child Component of Customers Component
export class sample extends Component {


    constructor(props) {
        super(props);
        municipalityTempList: []
      }
    

      componentDidMount () {
        this.getAPI();
        }


       getAPI= async() => {
                            const headers = {"headers":"Access-Control-Allow-Origin"}
                             await axios.all([ axios.get('https://10.11.27.104:3001/api/WebApi/GetCountry',{headers})])
                                             .then(axios.spread((data1) => {this.setState({ countriesList: data1.data}) } ))
                           }

       render() {
                        <div>
                            <Pane width="20%"marginRight={10}>
                                <SelectField
                                    name="countrypres"
                                    width="100%"
                                    description="Country">
                                    {this.state.countriesList.data.map((vcountries) => {
                                                    return <option value={vcountries.alpha2} > {vcountries.name} </option>  
                                    })}
                                
                                </SelectField>
                            </Pane>
                        </div>
            }

}