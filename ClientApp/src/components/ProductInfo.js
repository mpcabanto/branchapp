import React, { Component } from 'react';
import Panel from 'react-bootstrap/lib/Panel'
import axios from 'axios';
import { Pane, Text, Heading, TextInput, Select, RadioGroup, Combobox, Button, Paragraph } from 'evergreen-ui';
import { Link } from 'react-router-dom';
import './App.css';
import { Alert } from 'antd';
import SweetAlert from 'react-bootstrap-sweetalert';
import ReactDOM from 'react-dom';

//This Component is a child Component of Customers Component
export class ProductInfo extends Component {
  displayName = ProductInfo.name

  constructor(props) {
    super(props);
    this.state = {
      ProductType: "",
      popAlert: null,
      submittedjson:null,
      data: {}, errorResult: [],createaccountresponse: [],createaccountERRORresponse: [],
      displayTag:"MAIN"
    };
    this.changeview = this.loadproductview.bind(this)
  }




  togglePopup = (msg) => {
    const getAlert = () => (
      <SweetAlert
        success
        title="PSBank"
        onConfirm={() => this.hideAlert()}
      > {msg}
      </SweetAlert>
    );

    this.setState({
      popAlert: getAlert
    })
  }

  hideAlert() {
    this.setState({ popAlert: null })
  };


  loadproductview = x => {
    let newvalue = x.target.value;
    this.setState({displayTag: newvalue})
  };  


  uploadForm1 = (value, event) => {
    event.preventDefault();
    alert("Account created");
    const getAlert = () => (
      <SweetAlert
        success
        title="PSBank"
        onConfirm={() => this.hideAlert()}
      > Account Created.
  </SweetAlert>
    );

    this.setState({
      popAlert: getAlert()
    });
  };

  uploadForm = (value, event) => {
    event.preventDefault();
    this.setState({ ProductType: { value } });

    const config = {
      mode:"no-cors",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/x-www-form-urlencoded",
        "Accept": "application/json"
      }
    };

    const form1 = {
      id: this.props.location.state.userid,
      lastName: this.props.location.state.lastname,
      gender: this.props.location.state.gender,
      documentType: this.props.location.state.documenttype,
      mobileNumber: this.props.location.state.mobilenumber,
      referenceNumber: this.props.location.state.applicationrefno,
      civilStatus: this.props.location.state.civilstatus,
      type: "I",
      presentAddress: {
        zipCode: this.props.location.state.zipcodepres,
        country: this.props.location.state.countrypres,
        province: this.props.location.state.provincepres,
        city: this.props.location.state.citypres,
        address: this.props.location.state.streetpres
      },
      permanentAddress: {
        zipCode: this.props.location.state.zipcodeperm,
        country: this.props.location.state.countryperm,
        province: this.props.location.state.provinceperm,
        city: this.props.location.state.cityperm,
        address: this.props.location.state.streetperm
      },
      mailingAddress: {
        zipCode: this.props.location.state.zipcodeperm,
        country: this.props.location.state.countryperm,
        province: this.props.location.state.provinceperm,
        city: this.props.location.state.cityperm,
        address: this.props.location.state.streetperm
      },      
      incomeSource: this.props.location.state.sourceincome,
      //incomeSource:"SF1",
      emailAddress: this.props.location.state.email,
      productType: this.state.ProductType.value,
      monthlyIncome: this.props.location.state.monthlyincome,
      birthPlace: this.props.location.state.placeofbirth,
      profession: this.props.location.state.naturework,
      //profession: "REG",
      requestor: "TESTUSER",
      birthDate: this.props.location.state.birthdate,
      firstName: this.props.location.state.firstname,
      nationality: this.props.location.state.nationality,
      //nationality: "FN1",
      responsibilityCode: "111",
      middleName: this.props.location.state.middlename,
      spouseName: this.props.location.state.spousename,
      fatca: "NO",
      accountFacility: this.props.location.state.accountfacilities
    };
    console.log(form1);

    //axios.post('https://10.11.27.104:3001/api/webapi/createaccount', form1)
    //axios.post('http://localhost:44325/api/webapi/createaccount', form1)
    axios.post('https://10.11.27.104:3001/api/webapi/createaccount', form1)
      .then(response => {
        this.setState({ createaccountresponse: response.data});
        this.setState({ submittedjson: form1});
        console.log(this.state.createaccountresponse.errors);
       // console.log(form1);
        if (this.state.createaccountresponse.errors) {
          //alert(this.state.createaccountresponse.errors.value);
          alert(JSON.stringify(this.state.createaccountresponse.errors,null,4));
        } else {
          this.props.history.push({ pathname: '/endform', state: this.state.fields });
          alert(JSON.stringify(this.state.createaccountresponse,null,4));
        }
      })
      .catch((ex) => {
        console.error(ex);
        this.setState({ isError: true });
       alert(JSON.stringify(this.state.createaccountresponse.errors,null,4));
      });    


  }//end uplodForm










  renderMainProduct() {
    const { classes } = this.props;
    const { clientInfo } = this.state;
    if (this.state.displayTag == "MAIN") {
        return (
          <div>
        {/* 1st Line */}
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Link to={"https://psbmcgcasdev:9443/public-savings-menu/"} target="_blank">  <img src="/Savings.JPG" height="80" /> </Link>
          </Pane>
        </Pane>
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Heading size={900} color="#1070CA" >Savings</Heading>
          </Pane>
        </Pane>

        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Paragraph>Safeguard your money with our diverse types of savings accounts.</Paragraph>
          </Pane>
        </Pane>
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} height={100}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Button height={36} marginRight={16} appearance="primary" onClick={this.loadproductview} value={"SAVINGS"} >Apply Now</Button>
          </Pane>
        </Pane>

        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <img src="/Checking.JPG" height="80" />
          </Pane>
        </Pane>
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Heading size={900} color="#1070CA" >Checking</Heading>
          </Pane>
        </Pane>

        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Paragraph>Settle financial transactions with our convenient checking accounts.</Paragraph>
          </Pane>
        </Pane>
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} height={100}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Button height={36} marginRight={16} appearance="primary" onClick={this.loadproductview} value={"CHECKING"} >Apply Now</Button>
          </Pane>
        </Pane>


        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <img src="/TimeDeposit.JPG" height="80" />
          </Pane>
        </Pane>
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Heading size={900} color="#1070CA" >Time Deposit</Heading>
          </Pane>
        </Pane>

        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Paragraph>Meet your financial goal faster with our flexible time deposit products.</Paragraph>
          </Pane>
        </Pane>
        <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} height={100}>
          <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
            <Button height={36} marginRight={16} appearance="primary" onClick={this.loadproductview} value={"TD"} >Apply Now</Button>
          </Pane>
        </Pane>
      </div>

        );
    }
    else {
      
     }
}















renderSavings() {
  const { classes } = this.props;
  const { clientInfo } = this.state;
  if (this.state.displayTag == "SAVINGS") {
      return (
        <div>
        {/* 1st Line */}
        <Pane display="flex" paddingTop={0} background="tint2" borderRadius={3} elevation={0}>
          <Pane display="flex" width="100%" paddingTop={20} height="100%" alignItems="center" justifyContent="center">



              <Pane display="left" width="33%" alignItems="center" justifyContent="center"  bordercolor = "black">     
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Link to={"https://psbmcgcasdev:9443/public-savings-menu/"} target="_blank">  <img src="/savings_kiddie.png" height="80" /> </Link>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Heading size={400} color="#1070CA" >PSBank Kiddie and Teen Savers</Heading>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0}  margin={10}>
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                        <Heading size={200} color="#000000" align="center">Open your kids their own savings account with free insurance.</Heading>
                        </Pane>
                      </Pane>
                      <Pane display= "flex" paddingTop={6} background="tint2" borderRadius={0} height={100}>
                        <Pane display="flex" width="100%" height="15%" alignItems="top" justifyContent="center">
                          <Button height={25} width={100} justifyContent="center"  appearance="primary" onClick={this.uploadForm.bind(this, "901PHP")} >Apply Now</Button>
                        </Pane>
                      </Pane>
              </Pane>



              <Pane display="left" width="33%" alignItems="center" justifyContent="center"  bordercolor = "black">     

                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Link to={"https://psbmcgcasdev:9443/public-savings-menu/"} target="_blank">  <img src="/savings_atm.png" height="80" /> </Link>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Heading size={400} color="#1070CA" >PSBank ATM Savings</Heading>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0}  margin={10}>
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                        <Heading size={200} color="#000000" align="center">Have day and night access to your savings account via ATM.</Heading>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} height={100}>
                        <Pane display="flex" width="100%" height="15%" alignItems="center" justifyContent="center">
                        <Button height={25} width={100} justifyContent="center" appearance="primary" onClick={this.uploadForm.bind(this, "901PHP")} >Apply Now</Button>
                        </Pane>
                      </Pane>

              </Pane>


              <Pane display="right" width="33%" alignItems="center" justifyContent="center">     

                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Link to={"https://psbmcgcasdev:9443/public-savings-menu/"} target="_blank">  <img src="/savings_passbook.png" height="80" /> </Link>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Heading size={400} color="#1070CA" >PSBank Regular Passbook Savings</Heading>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0}  margin={10}>
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                        <Heading size={200} color="#000000" align="center">Easily track transactions with the convenience of a passbook.</Heading>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} height={100}>
                        <Pane display="flex" width="100%" height="15%" alignItems="center" justifyContent="center">
                        <Button height={25} width={100} justifyContent="center" appearance="primary" onClick={this.uploadForm.bind(this, "901PHP")} >Apply Now</Button>
                        </Pane>
                      </Pane>

              </Pane>
           </Pane>
        </Pane>




        {/* 1st Line */}
         <Pane display="flex" paddingTop={0} background="tint2" borderRadius={0}>
          <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">


              <Pane display="left" width="33%" alignItems="center" justifyContent="center">     

                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Link to={"https://psbmcgcasdev:9443/public-savings-menu/"} target="_blank">  <img src="/passbook-with-atm.png" height="80" /> </Link>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Heading size={400} color="#1070CA" >PSBank Passbook with ATM</Heading>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0}  margin={10}>
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                        <Heading size={200} color="#000000" align="center">Enjoy the convenience of a passbook and an ATM card in 1 account.</Heading>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} height={100}>
                        <Pane display="flex" width="100%" height="15%" alignItems="center" justifyContent="center">
                        <Button height={25} width={100} justifyContent="center" appearance="primary" onClick={this.uploadForm.bind(this, "901PHP")} >Apply Now</Button>
                        </Pane>
                      </Pane>

              </Pane>



               <Pane display="center" width="33%" alignItems="center" justifyContent="center">     

                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Link to={"https://psbmcgcasdev:9443/public-savings-menu/"} target="_blank">  <img src="/savings_dollar.png" height="80" /> </Link>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Heading size={400} color="#1070CA" >PSBank Dollar and Euro Savings</Heading>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0}  margin={10}>
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                        <Heading size={200} color="#000000" align="center">Be ready to see the world with easy access to your US dollar account.</Heading>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} height={100}>
                        <Pane display="flex" width="100%" height="15%" alignItems="center" justifyContent="center">
                        <Button height={25} width={100} justifyContent="center" appearance="primary" onClick={this.uploadForm.bind(this, "901PHP")} >Apply Now</Button>
                        </Pane>
                      </Pane>

              </Pane>


              <Pane display="right" width="33%" alignItems="center" justifyContent="center">     

                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Link to={"https://psbmcgcasdev:9443/public-savings-menu/"} target="_blank">  <img src="/savings_ofw.png" height="80" /> </Link>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Heading size={400} color="#1070CA" >PSBank Overseas Filipino Savings</Heading>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0}  margin={10}>
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                        <Heading size={200} color="#000000" align="center">Make cashless transactions worldwide with no maintaining balance.</Heading>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} height={100}>
                        <Pane display="flex" width="100%" height="15%" alignItems="center" justifyContent="center">
                        <Button height={25} width={100} justifyContent="center" appearance="primary" onClick={this.uploadForm.bind(this, "901PHP")} >Apply Now</Button>
                        </Pane>
                      </Pane>

              </Pane>


              
           </Pane>
        </Pane>

        <Pane align ="right"><Button height={20} width={75} justifyContent="center" appearance="primary" onClick={this.loadproductview} value={"MAIN"} >Back</Button> </Pane>
    </div>

      );
  }
  else {
    
   }
}












renderChecking() {
  const { classes } = this.props;
  const { clientInfo } = this.state;
  if (this.state.displayTag == "CHECKING") {
      return (
        <div>
        {/* 1st Line */}
        <Pane display="flex" paddingTop={0} background="tint2" borderRadius={3} elevation={0}>
          <Pane display="flex" width="100%" paddingTop={20} height="100%" alignItems="center" justifyContent="center">



              <Pane display="left" width="33%" alignItems="center" justifyContent="center"  bordercolor = "black">     
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Link to={"https://psbmcgcasdev:9443/public-savings-menu/"} target="_blank">  <img src="/checking_regular.png" height="80" /> </Link>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Heading size={400} color="#1070CA" >PSBank Regular Checking</Heading>
                        </Pane>
                      </Pane>
                      <Pane display= "flex" paddingTop={20} background="tint2" borderRadius={0} height={100}>
                        <Pane display="flex" width="100%" height="15%" alignItems="top" justifyContent="center">
                          <Button height={25} width={100} justifyContent="center"  appearance="primary" onClick={this.uploadForm.bind(this, "901PHP")} >Apply Now</Button>
                        </Pane>
                      </Pane>
              </Pane>



              <Pane display="left" width="33%" alignItems="center" justifyContent="center"  bordercolor = "black">     

                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Link to={"https://psbmcgcasdev:9443/public-savings-menu/"} target="_blank">  <img src="/checking_premium.png" height="80" /> </Link>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Heading size={400} color="#1070CA" >PSBank Premium Checking</Heading>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={20} background="tint2" borderRadius={0} height={100}>
                        <Pane display="flex" width="100%" height="15%" alignItems="center" justifyContent="center">
                        <Button height={25} width={100} justifyContent="center" appearance="primary" onClick={this.uploadForm.bind(this, "901PHP")} >Apply Now</Button>
                        </Pane>
                      </Pane>

              </Pane>

           </Pane>
        </Pane>
        <Pane align ="right"><Button height={20} width={75} justifyContent="center" appearance="primary" onClick={this.loadproductview} value={"MAIN"} >Back</Button> </Pane>
    </div>

      );
  }
  else {
    
   }
}






renderTD() {
  const { classes } = this.props;
  const { clientInfo } = this.state;
  if (this.state.displayTag == "TD") {
      return (
        <div>
        {/* 1st Line */}
        <Pane display="flex" paddingTop={0} background="tint2" borderRadius={3} elevation={0}>
          <Pane display="flex" width="100%" paddingTop={20} height="100%" alignItems="center" justifyContent="center">



              <Pane display="left" width="33%" alignItems="center" justifyContent="center"  bordercolor = "black">     
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Link to={"https://psbmcgcasdev:9443/public-savings-menu/"} target="_blank">  <img src="/td_prime.png" height="80" /> </Link>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Heading size={400} color="#1070CA" >PSBank Prime Time Deposit</Heading>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0}  margin={10}>
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                        <Heading size={200} color="#000000" align="center">Enjoy great returns with a high yield and tax-free placement.</Heading>
                        </Pane>
                      </Pane>
                      <Pane display= "flex" paddingTop={6} background="tint2" borderRadius={0} height={100}>
                        <Pane display="flex" width="100%" height="15%" alignItems="top" justifyContent="center">
                          <Button height={25} width={100} justifyContent="center"  appearance="primary" onClick={this.uploadForm.bind(this, "901PHP")} >Apply Now</Button>
                        </Pane>
                      </Pane>
              </Pane>



              <Pane display="left" width="33%" alignItems="center" justifyContent="center"  bordercolor = "black">     

                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Link to={"https://psbmcgcasdev:9443/public-savings-menu/"} target="_blank">  <img src="/td_peso.png" height="80" /> </Link>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Heading size={400} color="#1070CA" >PSBank Peso Time Deposit</Heading>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0}  margin={10}>
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                        <Heading size={200} color="#000000" align="center">Earn higher yields in as fast as 30 days for as low as Php10,000.</Heading>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} height={100}>
                        <Pane display="flex" width="100%" height="15%" alignItems="center" justifyContent="center">
                        <Button height={25} width={100} justifyContent="center" appearance="primary" onClick={this.uploadForm.bind(this, "901PHP")} >Apply Now</Button>
                        </Pane>
                      </Pane>

              </Pane>


              <Pane display="right" width="33%" alignItems="center" justifyContent="center">     

                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Link to={"https://psbmcgcasdev:9443/public-savings-menu/"} target="_blank">  <img src="/td_dollar.png" height="80" /> </Link>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} >
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                          <Heading size={400} color="#1070CA" >PSBank Dollar and Euro Time Deposit</Heading>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0}  margin={10}>
                        <Pane display="flex" width="100%" height="100%" alignItems="center" justifyContent="center">
                        <Heading size={200} color="#000000" align="center">Pump prime the future with a foreign currency time deposit.</Heading>
                        </Pane>
                      </Pane>
                      <Pane display="flex" paddingTop={6} background="tint2" borderRadius={0} height={100}>
                        <Pane display="flex" width="100%" height="15%" alignItems="center" justifyContent="center">
                        <Button height={25} width={100} justifyContent="center" appearance="primary" onClick={this.uploadForm.bind(this, "901PHP")} >Apply Now</Button>
                        </Pane>
                      </Pane>

              </Pane>
           </Pane>
        </Pane>

        <Pane align ="right"><Button height={20} width={75} justifyContent="center" appearance="primary" onClick={this.loadproductview} value={"MAIN"} >Back</Button> </Pane>
    </div>

      );
  }
  else {
    
   }
}






  render() {
    console.log(this.state);
    console.log(this.props.location.state);
    const { options, vFatka, vConfirmadd } = this.state;
    return (
      <div >

        {this.renderMainProduct()}
        {this.renderSavings()}
        {this.renderChecking()}
        {this.renderTD()}   























      </div>
    )
  }
}
