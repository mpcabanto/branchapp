﻿import ReactDOM from 'react-dom';
import React from 'react';
import { Link } from 'react-router-dom';
import { Glyphicon, Navbar, Nav, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import './NavMenu.css';
import psblogo from './PSBLogo.png';
import 'bootstrap/dist/css/bootstrap.min.css';
import { InputGroup, InputGroupButton, Button, Form, FormControl } from 'react-bootstrap';

export class NavMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = { navHeight: 10 };

    this.handleResize = this.handleResize.bind(this);
  }

  handleResize(e = null) {
    this.setState({ navHeight: ReactDOM.findDOMNode(this._navbar).offsetHeight });
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResize);
    this.handleResize();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  render() {
    return (
      <div style={{paddingTop: this.state.navHeight}}>

        <Navbar ref={(e) => this._navbar = e} fluid collapseOnSelect>
         <Navbar.Header>               
            <Navbar.Brand>
              <Link to="/"> Central Ops </Link>              
            </Navbar.Brand>
          <Navbar.Toggle/>
          </Navbar.Header>
        <Navbar.Collapse>
          <Nav >
            <LinkContainer to={'/'} exact>
              <NavItem>
                <Glyphicon glyph='home' /> Home
              </NavItem>
            </LinkContainer>
            <LinkContainer to={'/nacqueue'}>
              <NavItem>
                <Glyphicon glyph='th-list' /> NAC Review
              </NavItem>
            </LinkContainer>
            <LinkContainer to={'/FetchData'}>
              <NavItem>
                <Glyphicon glyph='th-list' /> NAC Approval
              </NavItem>
            </LinkContainer>
            <LinkContainer to={'/counter'}>
              <NavItem>
                <Glyphicon glyph='dashboard' /> Dashboard
              </NavItem>
            </LinkContainer>  
            <LinkContainer to={'/'} exact>
              <NavItem>
                <Glyphicon glyph='log-out' /> Logout
              </NavItem>
            </LinkContainer>
          </Nav>
          <Form inline className="center-item">
						<FormControl type="text" placeholder="Search" />
						<Button bsStyle="primary" className="search-button"> 
                <Glyphicon glyph='search' /></Button>
            </Form>
        </Navbar.Collapse>
        </Navbar>
      </div>

    );
  }
}

