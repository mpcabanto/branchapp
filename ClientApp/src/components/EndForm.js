import React, { Component, Fragment } from 'react';
import Secured from './Secured';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { Pane, Text, Heading, TextInput, Select, SelectField, RadioGroup, Combobox, Button } from 'evergreen-ui';

export class EndForm extends Component {
    displayName = EndForm.name


    render() {
        return (
            <div >
                <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1} height={200} >
                    <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
                        <Heading size={600} marginLeft={20} >Your application was successfully submitted.</Heading>
                    </Pane>
                </Pane>

                <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} elevation={1} height={200} >
                    <Pane display="flex" width="100%" alignItems="center" justifyContent="center">
                        <Heading size={400} marginLeft={20} >Thank you for taking the time to fill out  our digital form.</Heading>
                    </Pane>
                </Pane>
                <Pane display="flex" paddingTop={6} background="tint2" borderRadius={3} >
                    <Pane display="flex" width="100%" alignItems="center" justifyContent="center"> <Link to="/">
                        <Button height={36} marginRight={16} appearance="primary" >Home</Button> </Link>
                    </Pane>
                </Pane>
            </div>
        );
    }
}
